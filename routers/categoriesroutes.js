const express = require("express");
const auth = require("../middleware/auth")

const Router = express.Router();
const {
  storeCategories,
  getCategories,
  getCategoriesDetail,
  updateCategories,
  deleteCategories,
} = require('../controllers/categoriescontroller');

Router.get("/categories", getCategories);
Router.post("/categories", storeCategories);
Router.get("/categories/:id", getCategoriesDetail);
Router.put("/categories/:id",auth, updateCategories);
Router.delete("/categories/:id",auth, deleteCategories);

module.exports = Router;