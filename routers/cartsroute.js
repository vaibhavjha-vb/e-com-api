const express = require("express");
const auth = require("../middleware/auth")

const Router = express.Router();
const {
  storeCarts,
  getCarts,
  getCartsDetail,
  updateCarts,
  deleteCarts,
} = require('../controllers/cartscontroller');

Router.get("/carts", auth,getCarts);
Router.post("/carts", auth, storeCarts);
Router.get("/carts/:id", auth, getCartsDetail);
Router.put("/carts/:id", auth, updateCarts);
Router.delete("/carts/:id",auth, deleteCarts);

module.exports = Router;