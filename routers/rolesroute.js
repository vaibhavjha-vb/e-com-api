const express = require("express");
const auth = require("../middleware/auth")

const Router = express.Router();
const {
  storeRoles,
  getRoles,
  getRolesDetail,
  updateRoles,
  deleteRoles,
} = require('../controllers/rolescontroller');

Router.get("/roles", getRoles);
Router.post("/roles", storeRoles);
Router.get("/roles/:id", getRolesDetail);
Router.put("/roles/:id",auth, updateRoles);
Router.delete("/roles/:id",auth, deleteRoles);

module.exports = Router;