const express = require("express");
const auth = require("../middleware/auth")

const Router = express.Router();
const {
  storeOrders,
  getOrders,
  getOrdersDetail,
  updateOrders,
  deleteOrders,
} = require('../controllers/orderscontroller');

Router.get("/orders",auth, getOrders);
Router.post("/orders",auth, storeOrders);
Router.get("/orders/:id",auth, getOrdersDetail);
Router.put("/orders/:id", auth,updateOrders);
Router.delete("/orders/:id",auth, deleteOrders);

module.exports = Router;