const express = require("express");
const auth = require("../middleware/auth")

const Router = express.Router();
const {
  storeUser,
  getUsers,
  getuserDetail,
  updateuser,
  deleteUser,
} = require('../controllers/usercontroller');

Router.get("/users", getUsers);
Router.post("/users", storeUser);
Router.get("/users/:id",getuserDetail);
Router.put("/users/:id", auth,updateuser);
Router.delete("/users/:id", auth, deleteUser);

module.exports = Router;