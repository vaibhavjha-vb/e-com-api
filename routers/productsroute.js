const express = require("express");
const auth = require("../middleware/auth")

const Router = express.Router();

const {
  storeProducts,
  getProducts,
  getProductsDetail,
  updateProducts,
  deleteProducts,
  searchProducts
} = require('../controllers/productscontroller');

Router.get("/products", getProducts);
Router.post("/products", storeProducts);
Router.get("/products/:id", getProductsDetail);
Router.put("/products/:id",auth, updateProducts);
Router.delete("/products/:id",auth, deleteProducts);

module.exports = Router;