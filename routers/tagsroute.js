const express = require("express");
const auth = require("../middleware/auth")

const Router = express.Router();
const {
  storeTags,
  getTags,
  getTagsDetail,
  updateTags,
  deleteTags,
} = require('../controllers/tagscontroller');

Router.get("/tags", getTags);
Router.post("/tags", storeTags);
Router.get("/tags/:id", getTagsDetail);
Router.put("/tags/:id", auth,updateTags);
Router.delete("/tags/:id",auth, deleteTags);

module.exports = Router;