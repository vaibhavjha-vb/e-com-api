const express = require("express");
const auth = require("../middleware/auth");

const Router = express.Router();
const {
    login,
    logout,
} = require('../controllers/authenticateController');

Router.post("/login", login);
Router.put("/logout",auth,logout);

module.exports = Router;