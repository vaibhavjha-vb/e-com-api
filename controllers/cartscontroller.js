const cartsModal = require("../models/carts");

const storeCarts = async (req, res) => {
  try {
    const carts = await new cartsModal(req.body);
    carts.save();

    res.status(201).send(carts);
  } catch (error) {
    console.log(error);
  }
};

const getCarts = async (req, res) => {
  try {
    const carts = await cartsModal.find();
    res.status(200).send(carts);
  } catch (error) {
    console.log(error)
  }
};

const getCartsDetail = async (req, res) => {
  try {
    console.log(req.params.id)
    const carts = await cartsModal.find({_id:req.params.id});
    res.status(200).send(carts);

  } catch (error) {
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
  }
};

const updateCarts = async (req, res) => {
  try {
    console.log(req.params.id)
    const carts = await cartsModal.updateOne({_id:req.params.id},{$set:req.body});
    if(carts.modifiedCount===0){
      res.status(202).send("already updated");
    }
    else{
      res.status(202).send(carts);
    }  
  } catch (error) {
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error) 
  }
};

const deleteCarts = async (req, res) => {
  try {
    console.log(req.params.id)
    const carts = await cartsModal.deleteOne({_id:req.params.id});
    if(carts.deletedCount===0){
      res.status(200).send("id not found");
    }
    else{
      res.status(200).send(carts);
    }
  } catch (error) {
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)   
  }
};

module.exports = {
  storeCarts,
  getCarts,
  getCartsDetail,
  updateCarts,
  deleteCarts,
};