const categoriesModal = require("../models/categories");

const storeCategories = async (req, res) => {
  try {
    const categories = await new categoriesModal(req.body);
    categories.save();

    res.status(201).send(categories);
  } catch (error) {
    console.log(error);
  }
};

const getCategories = async (req, res) => {
  try {
    const categories = await categoriesModal.find();
    res.status(200).send(categories);
  } catch (error) {
    console.log1(error)
  }
};

const getCategoriesDetail = async (req, res) => {
  try {
    console.log(req.params.id)
    const categories = await categoriesModal.find({_id:req.params.id});

    res.status(200).send(categories);

  } catch (error) {
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
  }
};

const updateCategories = async (req, res) => {
  try {
    console.log(req.params.id)
    const categories = await categoriesModal.updateOne({_id:req.params.id},{$set:req.body});
    if(categories.modifiedCount===0){
      res.status(202).send("already updated");
    }
    else{
      res.status(202).send(categories);
    }

  } catch (error) {
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
  }
};

const deleteCategories = async (req, res) => {
  try {
    console.log(req.params.id)
    const categories = await categoriesModal.deleteOne({_id:req.params.id});
    if(categories.deletedCount===0){
      res.status(200).send("id not found");
    }
    else{
      res.status(200).send(categories);
    }
  } catch (error) {
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
  }
};

module.exports = {
  storeCategories,
  getCategories,
  getCategoriesDetail,
  updateCategories,
  deleteCategories,
};