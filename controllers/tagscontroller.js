const tagsModal = require("../models/tags");

const storeTags = async (req, res) => {
  try {
    const tags = await new tagsModal(req.body);
    tags.save();

    res.status(201).send(tags);
  }
  catch (error) {
    console.log(error);
  }
};

const getTags = async (req, res) => {
  try {
    const tags = await tagsModal.find();
    res.status(200).send(tags);
  } 
  catch (error) {
    console.log(error)
  }
};

const getTagsDetail = async (req, res) => {
  try{
    console.log(req.params.id)
    const tags = await tagsModal.find({_id:req.params.id});
    res.status(200).send(tags);
  }
  catch (error){
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
  }
};

const updateTags = async (req, res) => {
  try{ 
    console.log(req.params.id)
    const tags = await tagsModal.updateOne({_id:req.params.id},{$set:req.body});
    if(tags.modifiedCount===0){
      res.status(202).send("already updated");
    }
    else{
      res.status(202).send(tags);
    }
  }
  catch (error){
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
  }
};

const deleteTags = async (req, res) => {
  try{
    console.log(req.params.id)
    const tags = await tagsModal.deleteOne({_id:req.params.id});
    if(tags.deletedCount===0){
      res.status(200).send("id not found");
    }
    else{
      res.status(200).send(tags);
    }
  }
  catch (error){
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
  }
};

module.exports = {
  storeTags,
  getTags,
  getTagsDetail,
  updateTags,
  deleteTags,
};