const productsModal = require("../models/products");

const storeProducts = async (req, res) => {
  try {
    const products = await new productsModal(req.body);
    products.save();

    res.status(201).send(products);
  } catch (error) {
    console.log(error);
  }
};

const getProducts = async (req, res) => {
  try {
    let products;
    console.log(Object.keys(req.query).length);
    if(Object.keys(req.query).length===0){
      products = await productsModal.find();     
    }
    else{
      products = await productsModal.find(req.query);
      if(products.length===0){
        return res.status(400).send("no product found")
      }
    }
    res.status(200).send(products);
  } catch (error) {
    console.log(error)
  }
};

const getProductsDetail = async (req, res) => {
  try {
    console.log(req.params.id)
    const products = await productsModal.find({_id:req.params.id});
    res.status(200).send(products);
  } catch (error) {
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
  }
};

const updateProducts = async (req, res) => {
  try {
    console.log(req.params.id)
    const products = await productsModal.updateOne({_id:req.params.id},{$set:req.body});
    if(products.modifiedCount===0){
      res.status(202).send("already updated");
    }
    else{
      res.status(202).send(products);
    }
  } catch (error) {
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
  }
};

const deleteProducts = async (req, res) => {
  try {
    console.log(req.params.id)
    const products = await productsModal.deleteOne({_id:req.params.id});
    if(products.deletedCount===0){
      res.status(200).send("id not found");
    }
    else{
      res.status(200).send(products);
    }
  } catch (error) {
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
  }
};

const searchProducts = async(req, res)=>{
  console.log(req.query.category)
}
module.exports = {
  storeProducts,
  getProducts,
  getProductsDetail,
  updateProducts,
  deleteProducts,
  searchProducts,
};