const userModal = require("../models/user");

const storeUser = async (req, res) => {
  try {
    const useremail=await userModal.findOne({email:req.body.email});
    if(useremail){
      return res.status(400).send("user already exist");
    }
    
    const user = await new userModal(req.body);
    user.save();
    res.status(201).send(user);
  } catch (error) {
    console.log(error);
    res.status(500).send("user already exist")
  }
};

const getUsers = async (req, res) => {
  try {
    const users = await userModal.find();
    res.status(200).send(users);
  } catch (error) {
      console.log(error);
  }
};

const getuserDetail = async (req, res) => {
  try{
    console.log(req.params.id)
    const users = await userModal.find({_id:req.params.id});

    if(users.length===0){
      return res.status(404).send("Id not Found")
    }
    res.status(200).send(users);
  } catch (error){
      if(error.kind==='ObjectId'){
        return res.status(404).send("Id not Found");
    }
      console.log(error)
  }
};

const updateuser = async (req, res) => {
  try{ 
      console.log(req.params.id)
      const users = await userModal.updateOne({_id:req.params.id},{$set:req.body});
      if(users.modifiedCount===0){
        res.status(404).send("already updated");
      }
      else{
        res.status(202).send(users);
     }
  } catch (error){
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
  }
    console.log(error)
  }
};

const deleteUser = async (req, res) => {
  try{
    console.log(req.params.id)
    const users = await userModal.deleteOne({_id:req.params.id});
    if(users.deletedCount===0){
      res.status(200).send("id not found");
    }
    else{
      res.status(200).send(users);
    }
  } catch(error){
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
}
};

module.exports = {
  storeUser,
  getUsers,
  getuserDetail,
  updateuser,
  deleteUser,
};