const ordersModal = require("../models/orders");

const storeOrders = async (req, res) => {
  try {
    const orders = await new ordersModal(req.body);
    orders.save();

    res.status(201).send(orders);
  } catch (error) {
    console.log(error);
  }
};

const getOrders = async (req, res) => {
  try {
    const orders = await ordersModal.find();
    res.status(200).send(orders);
  } catch (error) {
    console.log(error)
  }
};

const getOrdersDetail = async (req, res) => {
  try {
    console.log(req.params.id)
    const orders = await ordersModal.find({_id:req.params.id});

    res.status(200).send(orders);

  } catch (error) {
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
  }
};

const updateOrders = async (req, res) => {
  try {
    console.log(req.params.id)
    const orders = await ordersModal.updateOne({_id:req.params.id},{$set:req.body});
    if(orders.modifiedCount===0){
      res.status(202).send("already updated");
    }
    else{
      res.status(202).send(orders);
    }
  } catch (error) {
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)    
  }
};

const deleteOrders = async (req, res) => {
  try {
    console.log(req.params.id)
    const orders = await ordersModal.deleteOne({_id:req.params.id});
    if(orders.deletedCount===0){
      res.status(200).send("id not found");
    }
    else{
      res.status(200).send(orders);
    }
  } catch (error) {
    if(error.kind==='ObjectId'){
      return res.status(404).send("Id not Found");
    } 
    console.log(error)
  }
};

module.exports = {
  storeOrders,
  getOrders,
  getOrdersDetail,
  updateOrders,
  deleteOrders,
};